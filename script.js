var notas = []

var calcMedia = () => {
    let media = 0
    for(let nota of notas){
        media += nota
    }
    return media / notas.length
}

var checkInput = (input) => {
    input = input.replace(",", ".")
    if(input == ""){
        window.alert("Por favor, insira uma nota.")
        return 0
    }
    else if(input <= 10 && input >= 0){
        notas.push(parseFloat(input))
        return 1
    }
    else{
        window.alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        return 0
    }
}

var btn = document.querySelector("#submit")
var btn2 = document.querySelector("#calc")

btn.addEventListener("click", () => {
    var inputNota = document.getElementById("nota")
    if(checkInput(inputNota.value)){
        document.querySelector("textarea").innerHTML += "A nota " + notas.length + " foi " + notas.at(-1) + "\r\n"
    }
    inputNota.value = ""
})

btn2.addEventListener("click", () => {
    document.getElementById("media-final").innerHTML = calcMedia().toFixed(2)
})